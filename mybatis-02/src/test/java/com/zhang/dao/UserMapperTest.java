package com.zhang.dao;


import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import com.zhang.pojo.User;
import com.zhang.util.MybatisUtils;

import java.util.List;

public class UserMapperTest {
    @Test
    public void test(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();

        //方法一
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);

        //方法二 (不推荐)
//        List<User> userList=sqlSession.selectList("com.zhang.dao.UserDao.getUserList");

        List<User> userList = userMapper.getUserList();

        for (User user : userList) {
            System.out.println(user);
        }

        sqlSession.close();
    }

    @Test
    public void testSelectById(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();

        //方法一
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);

        User user= userMapper.getUserById(1);

        System.out.println(user);

        sqlSession.close();
    }

    @Test
    public void addUser(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();

        //方法一
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);
        int result= userMapper.addUser(new User(4,"娃哈哈","123456"));
        if(result>0){
            System.out.println("插入成功！");
        }

        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void updateUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        int result = userMapper.updateUser(new User(4,"爽歪歪","2323232"));
        sqlSession.commit();
        if (result > 0) {
            System.out.println("修改成功！");
        }
        sqlSession.close();

    }

    @Test
    public void deleteUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        int result = userMapper.deleteUser(4);
        if (result > 0) {
            System.out.println("删除成功！");

        }
        sqlSession.commit();
        sqlSession.close();
    }


}
