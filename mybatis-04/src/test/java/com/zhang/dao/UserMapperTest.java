package com.zhang.dao;


import com.zhang.pojo.User;
import com.zhang.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;


public class UserMapperTest {

    /*查找所有*/
    @Test
    public void testSelectall(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();
        //本质上利用了jvm的动态代理机制
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);

        List<User> users= userMapper.getAllUsers();
        for (User user : users) {
            System.out.println(user);
        }
        sqlSession.close();
    }

    @Test
    public void testSelectById(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();
        //本质上利用了jvm的动态代理机制
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);

        User user= userMapper.getUserById(1);
        System.out.println(user);
        sqlSession.close();
    }

    /*插入*/
    @Test
    public void testInsertUser(){
        /*直接调用重载后的接口 自动事务提交 不用commit了*/
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        mapper.addUser(new User(6,"大刀","66666666"));
//        sqlSession.commit();
        sqlSession.close();
    }

    /*更新*/
    @Test
    public void testUpdateUser(){
        /*直接调用重载后的接口 自动事务提交 不用commit了*/
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        mapper.updateUser(new User(6,"令狐冲","55555555"));
//        sqlSession.commit();
        sqlSession.close();
    }

    /*删除*/
    @Test
    public void testDeleteUser(){
        /*直接调用重载后的接口 自动事务提交 不用commit了*/
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        mapper.deleteUser(6);
//        sqlSession.commit();
        sqlSession.close();
    }
}
