package com.zhang.dao;



import com.zhang.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 使用了注解就不需要对应的Mapper.xml文件了
 */
public interface UserMapper {
    /*通过id查找*/
    @Select("select id,name,pwd as password from mybatis.user where id=#{id}")
    User getUserById(@Param("id") int id);

    /*查找所有*/
    @Select("select id,name,pwd as password from mybatis.user ")
    List<User> getAllUsers();

    /*插入*/
    @Insert("insert into user(id,name,pwd) values(#{id},#{name},#{password})")
    int addUser(User user);

    /*更新*/
    @Update("update user set name=#{name},pwd=#{password} where id=#{id}")
    int updateUser(User user);
    @Delete("delete from user where id=#{id}")
    int deleteUser(@Param("id") int id);
}
