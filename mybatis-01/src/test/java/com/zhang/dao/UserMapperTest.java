package com.zhang.dao;

import com.zhang.pojo.User;
import com.zhang.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserMapperTest {

    /*获取所有*/
    @Test
    public void testGetAll(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();//静态方法直接通过类名调用

        //方法一
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);//通过sqlSession获取相应类映射操作

        //方法二 (不推荐)
//        List<User> userList=sqlSession.selectList("com.zhang.dao.UserDao.getUserList");

        /*操作时本质还是调用接口函数，这个接口函数就是原来的dao操作，接口函数里本质调用sql语句*/
        List<User> userList = userMapper.getUserList();
        for (User user : userList) {
            System.out.println(user);
        }
        sqlSession.close();
    }

    /*通过id查找*/
    @Test
    public void testSelectById(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);//获取接口对象
        User user= userMapper.getUserById(1);//调用相应接口函数
        System.out.println(user);
        sqlSession.close();
    }

    /*插入整个对象*/
    @Test
    public void addUser(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);
        int result= userMapper.addUser(new User(4,"娃哈哈4","123456"));
        if(result>0){
            System.out.println("插入成功！");
        }

        sqlSession.commit();//对数据库有修改操作的都需要调用commit
        sqlSession.close();
    }

    /*通过id更新，传递整个对象就行*/
    @Test
    public void updateUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        int result = userMapper.updateUser(new User(4,"爽歪歪","2323232"));
        sqlSession.commit();//对数据库有修改操作的都需要调用commit
        if (result > 0) {
            System.out.println("修改成功！");
        }
        sqlSession.close();

    }

    @Test
    public void deleteUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        int result = userMapper.deleteUser(4);
        if (result > 0) {
            System.out.println("删除成功！");

        }
        sqlSession.commit();
        sqlSession.close();
    }

    /*通过键值对方式添加，可以避免属性名不一致问题*/
    @Test
    public void addUser2(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("userid",5);
        map.put("username","zhangsan");
        map.put("userpwd","1233456");
        int result = userMapper.addUser2(map);
        if (result > 0) {
            System.out.println("添加2成功！");

        }
        sqlSession.commit();//对数据进行了修改就要提交
        sqlSession.close();
    }
}
