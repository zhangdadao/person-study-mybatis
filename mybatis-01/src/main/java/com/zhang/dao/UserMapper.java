package com.zhang.dao;

import com.zhang.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * 这里都是一些数据库操作的接口
 */
public interface UserMapper {
    /*获取所有*/
    List<User> getUserList();

    /*通过ID查*/
    User getUserById(int id);

    /*添加整个对象，在SQL语句中设置属性映射*/
    int addUser(User user);

    /**/
    int addUser2(Map<String,Object> map);

    /*更新*/
    int updateUser(User user);

    /*删除*/
    int deleteUser(int id);
}
