package com.zhang.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

//sqlSessionFaction-->sqlSession
public class MybatisUtils {

    private static SqlSessionFactory sqlSessionFactory;//相当于一个数据库的连接池，可以用来创建SqlSession接口对象

    //获取sqlSessionFaction
    static {
        try {
            String resource = "mybatis-config.xml";
            InputStream inputStream = Resources.getResourceAsStream(resource);//读取配置文件
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);//通过SqlSessionFactoryBuilder创建sql工厂
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //获取sqlSession  通过获取的sqlSessionFaction来获取sqlSession
    public static SqlSession getSqlSession(){
        return sqlSessionFactory.openSession();
    }//相当于一个连接对象

}
