package com.zhang.dao;



import com.zhang.pojo.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    User getUserById(int id);


    List<User> getUserByLimiters(Map<String, Object> map);
}
