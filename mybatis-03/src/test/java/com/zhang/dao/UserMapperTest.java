package com.zhang.dao;


import com.zhang.pojo.User;
import com.zhang.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserMapperTest {

    //获取日志对象
    static Logger logger=Logger.getLogger(UserMapperTest.class);

    @Test
    public void testSelectById(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);
        logger.info("info进入testSelectById");//信息级别的日志
        User user= userMapper.getUserById(1);
        System.out.println(user);
        sqlSession.close();
    }

    @Test
    public void getUserLimit(){
        //获取SqlSession
        SqlSession sqlSession= MybatisUtils.getSqlSession();

        //方法一
        UserMapper userMapper =sqlSession.getMapper(UserMapper.class);

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("first",0);
        map.put("last",2);
        List<User> users= userMapper.getUserByLimiters(map);
        for (User user : users) {
            System.out.println(user);
        }

        sqlSession.close();
    }

    @Test
    public void testLog4j(){

        //可以通过这个方式实现找错
        logger.info("info进入testLog4j");
        logger.error("erro进入testLog4j");
        logger.debug("debug进入testLog4j");
    }


}
